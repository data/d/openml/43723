# OpenML dataset: Toronto-Apartment-Rental-Price

https://www.openml.org/d/43723

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
I have collected the Toronto Apartment Rental prices from various sources in local websites. 
Content
There are 7 columns in the dataset.
Bedroom - How many bedrooms available
Bathroom - How many bathrooms available
Den - Whether den is available or not
Address - Location
Lat - Lattitude
Long - Longitude
Price - Apartment Rental price per month in CAD
Inspiration
I would recommend you to collect more insights from these data and show some visual as well. Predicting the price would be great.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43723) of an [OpenML dataset](https://www.openml.org/d/43723). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43723/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43723/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43723/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

